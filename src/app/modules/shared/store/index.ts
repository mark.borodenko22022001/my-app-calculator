import { ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

export function log(reducer: ActionReducer<object>): ActionReducer<object> {
  return (state, action) => {
    console.log('state', state);
    console.log('action', action);
    console.log('\n');
    return reducer(state, action);
  };
}

const localStorageSyncReducer = localStorageSync({
  keys: ['calculator'],
  rehydrate: true,
});

export const metaReducers: MetaReducer<object>[] = [
  log,
  localStorageSyncReducer,
];

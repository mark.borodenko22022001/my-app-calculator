import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { AgGridModule } from 'ag-grid-angular';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { calculatorReducer } from './store/calculator.reducer';
import { CalculatorEffects } from './store/calculator.effects';
import { CalculatorRoutingModule } from './calculator-routing.module';
import { StoreFeatureConst } from '../shared/consts/store-feature.const';
import { HistoryComponent } from './components/history/history.component';
import { CalculatorComponent } from './components/calculator/calculator.component';

@NgModule({
  declarations: [HistoryComponent, CalculatorComponent],
  imports: [
    FormsModule,
    CommonModule,
    AgGridModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    CalculatorRoutingModule,
    EffectsModule.forFeature([CalculatorEffects]),
    StoreModule.forFeature(StoreFeatureConst.CALCULATOR, calculatorReducer),
  ],
})
export class CalculatorModule {}

import { createReducer, on } from '@ngrx/store';
import * as CalculatorActions from './calculator.action';

export interface CalculatorState {
  input: string;
  history: { task: string; result: string }[];
}

const initialState: CalculatorState = {
  input: '',
  history: [],
};

export const calculatorReducer = createReducer(
  initialState,
  on(CalculatorActions.updateInput, (state, { input }) => ({
    ...state,
    input,
  })),
  on(CalculatorActions.saveCalculation, (state, { task, result }) => ({
    ...state,
    input: '',
    history: [...state.history, { task, result }],
  })),
  on(CalculatorActions.clearHistory, (state) => ({
    ...state,
    history: [],
  })),
);

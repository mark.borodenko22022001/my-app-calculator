import { createAction, props } from '@ngrx/store';

export const clearHistory = createAction('[Calculator] Clear History');
export const updateInput = createAction(
  '[Calculator] Update Input',
  props<{ input: string }>(),
);
export const saveCalculation = createAction(
  '[Calculator] Save Calculation',
  props<{ task: string; result: string }>(),
);

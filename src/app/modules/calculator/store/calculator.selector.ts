import { CalculatorState } from './calculator.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { StoreFeatureConst } from '../../shared/consts/store-feature.const';

export const selectCalculatorState = createFeatureSelector<CalculatorState>(
  StoreFeatureConst.CALCULATOR,
);
export const selectInput = createSelector(
  selectCalculatorState,
  (state) => state.input,
);
export const selectHistory = createSelector(
  selectCalculatorState,
  (state) => state.history,
);

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as CalculatorActions from '../../store/calculator.action';
import { selectInput, selectHistory } from '../../store/calculator.selector';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent {
  @ViewChild('ingCalc', { static: false })
  private _ingCalc: ElementRef<HTMLDivElement> | undefined;

  toShow: string;
  numbers: number[];
  mycalcNgClass: string;

  input$: Observable<string>;
  history$: Observable<{ task: string; result: string }[]>;

  private _isIngCalcVisible = false;
  private lastOperator = '';

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private store: Store,
  ) {
    this.toShow = '';
    this.mycalcNgClass = '';
    this.numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

    this.input$ = this.store.select(selectInput);
    this.history$ = this.store.select(selectHistory);
  }

  updateInput(input: string) {
    this.store.dispatch(CalculatorActions.updateInput({ input }));
  }

  isOperator(value: string): boolean {
    return ['+', '-', '*', '/'].includes(value);
  }

  isLastCharacterOperator(): boolean {
    const lastCharacter = this.toShow.charAt(this.toShow.length - 1);
    return this.isOperator(lastCharacter);
  }

  writeToInput(value: number | string): void {
    const operatorsRegex = /[-+/*]{2,}/g;
    const newInput = this.toShow + value;

    if (
      !newInput.match(operatorsRegex) &&
      !(
        this.toShow === '' &&
        (value === '-' || ['+', '*', '/'].includes(value as string))
      ) &&
      !(
        typeof value === 'string' &&
        this.isOperator(value as string) &&
        this.isLastCharacterOperator()
      )
    ) {
      this.toShow += value;
    }
  }

  clear(): void {
    this.toShow = '';
  }

  back(): void {
    if (this.toShow.length > 0) {
      this.toShow = this.toShow.slice(0, -1);
    }
  }

  correctBracket(input: string): boolean {
    let leftBracket = 0;
    for (let i = 0; i < input.length; i++) {
      if (input[i] === '(') {
        leftBracket++;
      } else if (input[i] === ')') {
        if (leftBracket === 0) {
          return false;
        }
        leftBracket--;
      }
    }
    return leftBracket === 0;
  }

  calcValue(solve: string): void {
    if (!this.correctBracket(solve)) {
      this.toShow = 'Invalid expression';
      return;
    }

    if (solve.charAt(0) == '0') {
      solve = solve.slice(1);
    }
    solve = solve
      .replaceAll('sin', 'Math.sin')
      .replaceAll('cos', 'Math.cos')
      .replaceAll('tan', 'Math.tan')
      .replaceAll('sqrt', 'Math.sqrt')
      .replaceAll('log', 'Math.log')
      .replaceAll('π', 'Math.PI')
      .replaceAll('e', 'Math.E');

    const task: string = solve;

    while (this.isLastCharacterOperator()) {
      this.toShow = this.toShow.slice(0, -1);
    }

    this.toShow = String(eval(solve));
    this.store.dispatch(
      CalculatorActions.saveCalculation({ task, result: this.toShow }),
    );
  }

  changeOperator(operator: string): void {
    const lastCharacter = this.toShow.charAt(this.toShow.length - 1);

    if (this.isOperator(lastCharacter)) {
      this.toShow = this.toShow.slice(0, -1) + operator;
    } else {
      this.toShow += operator;
    }
  }

  fact(): void {
    let f = 1;
    const num: number = parseFloat(this.toShow);
    for (let i = 1; i <= num; i++) {
      f = f * i;
    }
    this.toShow = f.toString();
  }

  power(): void {
    this.toShow = Math.pow(parseFloat(this.toShow), 2).toString();
  }

  toggleIngCalc(): void {
    if (this._ingCalc) {
      const isIngCalcVisible = !this._isIngCalcVisible;
      if (isIngCalcVisible) {
        this._ingCalc.nativeElement.style.display = 'flex';
        this.mycalcNgClass = 'my-ing-calc';
        setTimeout(() => {
          this._isIngCalcVisible = isIngCalcVisible;
          if (this._ingCalc) {
            this._ingCalc.nativeElement.style.position = 'relative';
            this._ingCalc.nativeElement.style.opacity = '1';
          }
        }, 500);
      } else {
        this._isIngCalcVisible = isIngCalcVisible;
        this._ingCalc.nativeElement.style.opacity = '0';
        setTimeout(() => {
          if (this._ingCalc) {
            this._ingCalc.nativeElement.style.display = 'none';
            this._ingCalc.nativeElement.style.position = 'absolute';
          }
          this.mycalcNgClass = '';
        }, 500);
      }
    }
  }
}

// import { Observable } from 'rxjs';
// import { Store } from '@ngrx/store';
// import * as CalculatorActions from '../../store/calculator.action';
// import { selectInput, selectHistory } from '../../store/calculator.selector';
// import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';

// @Component({
//   selector: 'app-calculator',
//   templateUrl: './calculator.component.html',
//   styleUrls: ['./calculator.component.scss'],
// })

// export class CalculatorComponent {
//   @ViewChild('ingCalc', { static: false })
//   private _ingCalc: ElementRef<HTMLDivElement> | undefined;

//   toShow: string;
//   numbers: number[];
//   mycalcNgClass: string;

//   input$: Observable<string>;
//   history$: Observable<{ task: string, result: string }[]>;

//   private _isIngCalcVisible: boolean = false;
//   private lastOperator: string = '';

//   constructor(private _changeDetectorRef: ChangeDetectorRef, private store: Store) {
//     this.toShow = '';
//     this.mycalcNgClass = '';
//     this.numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

//     this.input$ = this.store.select(selectInput);
//     this.history$ = this.store.select(selectHistory);
//   }

//   updateInput(input: string) {
//     this.store.dispatch(CalculatorActions.updateInput({ input }));
//   }

//   isOperator(value: string): boolean {
//     return ['+', '-', '*', '/'].includes(value);
//   }

//   isLastCharacterOperator(): boolean {
//     const lastCharacter = this.toShow.charAt(this.toShow.length - 1);
//     return this.isOperator(lastCharacter);
//   }

//   writeToInput(value: number | string): void {
//     const operatorsRegex = /[-+/*]{2,}/g;
//     const newInput = this.toShow + value;

//     if (
//       !newInput.match(operatorsRegex) &&
//       !(
//         this.toShow === '' &&
//         ['+', '*', '/'].includes(value as string)
//       ) &&
//       !(
//         this.toShow === '' &&
//         value === '-' &&
//         !this.isOperator(this.toShow.charAt(this.toShow.length - 1))
//       ) &&
//       !(
//         typeof value === 'string' &&
//         this.isOperator(value as string) &&
//         this.isLastCharacterOperator()
//       )
//     ) {
//       if (this.toShow === '' && value === '-') {
//         this.toShow += value;
//       } else {
//         this.toShow += value;
//       }
//     }
//   }

//   clear(): void {
//     this.toShow = '';
//   }

//   back(): void {
//     if (this.toShow.length > 0) {
//       this.toShow = this.toShow.slice(0, -1);
//     }
//   }

//   correctBracket(input: string): boolean {
//     let leftBracket: number = 0;
//     for (let i = 0; i < input.length; i++) {
//       if (input[i] === "(") {
//         leftBracket++;
//       } else if (input[i] === ")") {
//         if (leftBracket === 0) {
//           return false;
//         }
//         leftBracket--;
//       }
//     }
//     return leftBracket === 0;
//   }

//   calcValue(solve: string): void {
//     if (!this.correctBracket(solve)) {
//       this.toShow = "Invalid expression";
//       return;
//     }

//     if (solve.charAt(0) == '0') {
//       solve = solve.slice(1);
//     }
//     solve = solve
//       .replaceAll('sin', 'Math.sin')
//       .replaceAll('cos', 'Math.cos')
//       .replaceAll('tan', 'Math.tan')
//       .replaceAll('sqrt', 'Math.sqrt')
//       .replaceAll('log', 'Math.log')
//       .replaceAll('π', 'Math.PI')
//       .replaceAll('e', 'Math.E');

//     const task: string = solve;

//     while (this.isLastCharacterOperator()) {
//       this.toShow = this.toShow.slice(0, -1);
//     }

//     this.toShow = String(eval(solve));
//     this.store.dispatch(CalculatorActions.saveCalculation({ task, result: this.toShow }));
//   }

//   changeOperator(operator: string): void {
//     const lastCharacter = this.toShow.charAt(this.toShow.length - 1);

//     if (
//       this.toShow !== '' &&
//       this.isOperator(lastCharacter) &&
//       this.isOperator(operator)
//     ) {
//       this.toShow = this.toShow.slice(0, -1) + operator;
//     } else if (
//       this.toShow !== '' &&
//       !this.isOperator(lastCharacter) &&
//       this.isOperator(operator)
//     ) {
//       this.toShow += operator;
//     }
//   }

//   fact(): void {
//     let f: number = 1;
//     let num: number = parseFloat(this.toShow);
//     for (let i = 1; i <= num; i++) {
//       f = f * i;
//     }
//     this.toShow = f.toString();
//   }

//   power(): void {
//     this.toShow = Math.pow(parseFloat(this.toShow), 2).toString();
//   }

//   toggleIngCalc(): void {
//     if (this._ingCalc) {
//       const isIngCalcVisible: boolean = !this._isIngCalcVisible;
//       if (isIngCalcVisible) {
//         this._ingCalc.nativeElement.style.display = 'flex';
//         this.mycalcNgClass = 'my-ing-calc';
//         setTimeout(() => {
//           this._isIngCalcVisible = isIngCalcVisible;
//           if (this._ingCalc) {
//             this._ingCalc.nativeElement.style.position = 'relative';
//             this._ingCalc.nativeElement.style.opacity = '1';
//           }
//         }, 500);
//       } else {
//         this._isIngCalcVisible = isIngCalcVisible;
//         this._ingCalc.nativeElement.style.opacity = '0';
//         setTimeout(() => {
//           if (this._ingCalc) {
//             this._ingCalc.nativeElement.style.display = 'none';
//             this._ingCalc.nativeElement.style.position = 'absolute';
//           }
//           this.mycalcNgClass = '';
//         }, 500);
//       }
//     }
//   }
// }

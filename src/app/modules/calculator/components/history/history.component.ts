// import { Observable } from 'rxjs';
// import { Store } from '@ngrx/store';
// import { clearHistory } from '../../store/calculator.action';
// import { selectHistory } from '../../store/calculator.selector';
// import { GridOptions, GridApi, ColDef } from 'ag-grid-community';
// import { AfterViewInit, ViewChild, Component } from '@angular/core';

// @Component({
//   selector: 'app-history',
//   templateUrl: './history.component.html',
//   styleUrls: ['./history.component.scss'],
// })
// export class HistoryComponent implements AfterViewInit {
//   @ViewChild('agGrid') agGrid: any;
//   columnDefs: ColDef[];
//   defaultColDef: ColDef;
//   gridOptions: GridOptions;
//   showCalculations = true;
//   calculations$: Observable<{ task: string; result: string }[]>;
//   history: any[] = [];
//   private gridApi!: GridApi;

//   constructor(private store: Store) {
//     this.calculations$ = this.store.select(selectHistory);
//     this.columnDefs = [
//       { headerName: 'Task', field: 'task' },
//       {
//         headerName: 'Result',
//         field: 'result',
//         filter: 'agTextColumnFilter',
//         sortable: true,
//         sortingOrder: ['asc', 'desc'],
//         comparator: this.resultComparator,
//       },
//     ];

//     this.defaultColDef = {
//       sortable: true,
//       filter: true,
//     };

//     this.gridOptions = {};
//   }

//   clearHistory(): void {
//     this.store.dispatch(clearHistory());
//   }

//   onGridReady(params: any): void {
//     this.gridApi = params.api;
//     this.gridApi.sizeColumnsToFit();
//   }

//   ngAfterViewInit(): void {
//     this.gridOptions.api?.setRowData([]);
//   }

//   resultComparator(
//     valueA: string,
//     valueB: string,
//     nodeA: any,
//     nodeB: any,
//     isInverted: boolean,
//   ): number {
//     const a = parseFloat(valueA);
//     const b = parseFloat(valueB);
//     if (a === b) {
//       return 0;
//     }
//     return a < b ? -1 : 1;
//   }
// }

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { GridReadyEvent } from 'ag-grid-community';
import { clearHistory } from '../../store/calculator.action';
import { selectHistory } from '../../store/calculator.selector';
import { GridOptions, GridApi, ColDef } from 'ag-grid-community';
import { AfterViewInit, ViewChild, Component } from '@angular/core';

interface HistoryItem {
  task: string;
  result: string;
}

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements AfterViewInit {
  @ViewChild('agGrid', { static: false }) agGrid: Element | undefined;
  columnDefs: ColDef[];
  defaultColDef: ColDef;
  gridOptions: GridOptions;
  showCalculations = true;
  calculations$: Observable<HistoryItem[]>;
  history: HistoryItem[] = [];
  private gridApi!: GridApi;

  constructor(private store: Store) {
    this.calculations$ = this.store.select(selectHistory);
    this.columnDefs = [
      { headerName: 'Task', field: 'task' },
      {
        headerName: 'Result',
        field: 'result',
        filter: 'agTextColumnFilter',
        sortable: true,
        sortingOrder: ['asc', 'desc'],
        comparator: this.resultComparator,
      },
    ];

    this.defaultColDef = {
      sortable: true,
      filter: true,
    };

    this.gridOptions = {};
  }

  clearHistory(): void {
    this.store.dispatch(clearHistory());
  }

  onGridReady(params: GridReadyEvent): void {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }

  ngAfterViewInit(): void {
    this.gridOptions.api?.setRowData([]);
  }

  resultComparator(valueA: string, valueB: string): number {
    const a = parseFloat(valueA);
    const b = parseFloat(valueB);
    if (a === b) {
      return 0;
    }
    return a < b ? -1 : 1;
  }
}

import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouteConst } from 'src/app/modules/shared/consts/routes/route.const';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  currentRoute: string;

  private _isButtonClicked: boolean;
  private _subscription: Subscription;

  constructor(public router: Router) {
    this._isButtonClicked = false;
    this.currentRoute = this.router.url;
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    this._subscription.add(
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.currentRoute = event.url;
        }
      }),
    );
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  onIconBtnClick(): void {
    this.router.navigateByUrl(
      this.currentRoute === `/${RouteConst.CALCULATOR_HISTORY}`
        ? RouteConst.CALCULATOR
        : RouteConst.CALCULATOR_HISTORY,
    );
  }

  onButtonClick(): void {
    this._isButtonClicked = true;
  }

  isRouteActive(route: string): boolean {
    return this.currentRoute === route && this._isButtonClicked;
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteConst } from '../shared/consts/routes/route.const';
import { BaseLayoutComponent } from './components/base-layout/base-layout.component';

const routes: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      {
        path: RouteConst.CALCULATOR,
        loadChildren: () =>
          import('../calculator/calculator.module').then(
            (m) => m.CalculatorModule,
          ),
      },
      {
        path: '',
        redirectTo: RouteConst.CALCULATOR,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}

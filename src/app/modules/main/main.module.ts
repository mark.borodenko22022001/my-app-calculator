import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MainRoutingModule } from './main-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BaseLayoutComponent } from './components/base-layout/base-layout.component';

@NgModule({
  declarations: [FooterComponent, HeaderComponent, BaseLayoutComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule, MainRoutingModule],
})
export class MainModule {
  constructor(private router: Router) {}
}
